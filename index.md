---
layout: page
title: Jason Kridner
---

![Build Status](https://git.beagleboard.org/jkridner/jkridner.beagleboard.io/badges/main/pipeline.svg)

# [jkridner.beagleboard.io](https://jkridner.beagleboard.io)

Simple page to link to my various OpenBeagle pages.

* [docs.beagleboard.io](https://jkridner.beagleboard.io/docs)
* [gsoc.beagleboard.io](https://jkridner.beagleboard.io/gsoc)
* [gateware](https://jkridner.beagleboard.io/gateware)

## Posts

{% for post in site.posts %}
* <span class="post-meta">{{ post.date | date: "%b %-d, %Y" }}</span> [{{ post.title }}]({{ post.url | prepend: site.baseurl }})
{% endfor %}

## Feed

* <span class="rss-subscribe">[Subscribe via RSS]({{ "/feed.xml" | prepend: site.baseurl }})</span>
